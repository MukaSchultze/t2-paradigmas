package t4;

import java.util.ArrayList;

public class DaoGenerator implements Runnable {

    private TableProxy table;
    private String directory;

    public DaoGenerator(TableProxy table, String directory) {
        this.table = table;
        this.directory = directory;
    }

    private void joinNamesForSQL(StringBuilder builder, ArrayList<ColumnProxy> columns, String format) {
        for (int i = 0; i < columns.size(); i++) {
            builder.append(String.format(format, columns.get(i).getName()));

            if (i != columns.size() - 1)
                builder.append(", ");
        }
    }

    private String getTValue() {
        return Utility.pascalCase(this.table.getTableName());
    }

    private String getTKey() throws Exception {
        return this.table.getPrimaryKeyColumn().getJavaTypeName();
    }

    @Override
    public void run() {
        try {
            var pack = Utility.packageFromDirectory(directory);
            var str = new StringBuilder();

            str.append("package ");
            str.append(pack);
            str.append(";\n\n");

            str.append("import java.sql.PreparedStatement;\n");
            str.append("import java.sql.ResultSet;\n");
            str.append("import java.sql.SQLException;\n");
            str.append("import java.sql.Types;\n\n");
            str.append("import javax.annotation.processing.Generated;\n\n");
            str.append("import t4.DAO;\n\n");

            str.append("@Generated(\"t4.DaoGenerator\")\n");
            str.append("public class ");

            str.append(Utility.pascalCase(this.table.getTableName()));
            str.append("DAO extends DAO<");
            str.append(getTValue());
            str.append(", ");
            str.append(getTKey());
            str.append("> {\n\n");

            // Select
            str.append("    @Override\n");
            str.append("    protected String getSelectSQL() {\n        return \"");
            str.append("SELECT * FROM ");
            str.append(table.getTableName());
            str.append(" WHERE ");
            str.append(table.getPrimaryKeyColumn().getName());
            str.append(" = ?");
            str.append("\";\n    }\n\n");

            // Select all
            str.append("    @Override\n");
            str.append("    protected String getSelectAllSQL() {\n        return \"");
            str.append("SELECT * FROM ");
            str.append(table.getTableName());
            str.append("\";\n    }\n\n");

            // Insert
            str.append("    @Override\n");
            str.append("    protected String getInsertSQL() {\n        return \"");
            str.append("INSERT INTO ");
            str.append(table.getTableName());
            str.append(" (");
            joinNamesForSQL(str, table.getColumns(), "%s");
            str.append(") VALUES (");
            joinNamesForSQL(str, table.getColumns(), "?");
            str.append(")\";\n    }\n\n");

            // Update
            str.append("    @Override\n");
            str.append("    protected String getUpdateSQL() {\n        return \"");
            str.append("UPDATE ");
            str.append(table.getTableName());
            str.append(" SET ");
            joinNamesForSQL(str, table.getColumns(), "%s = ?");
            str.append(" WHERE ");
            str.append(table.getPrimaryKeyColumn().getName());
            str.append(" = ?");
            str.append("\";\n    }\n\n");

            // Delete
            str.append("    @Override\n");
            str.append("    protected String getDeleteSQL() {\n        return \"");
            str.append("DELETE FROM ");
            str.append(table.getTableName());
            str.append(" WHERE ");
            str.append(table.getPrimaryKeyColumn().getName());
            str.append(" = ?");
            str.append("\";\n    }\n\n");

            // FillSelectStatement
            str.append("    @Override\n");
            str.append("    protected void fillSelectStatement(PreparedStatement ps, ");
            str.append(getTKey());
            str.append(" key) throws SQLException {\n");
            str.append("        ps.setObject(1, key, Types.");
            str.append(table.getPrimaryKeyColumn().getType());
            str.append(");\n    }\n\n");

            // FillInsertStatement
            var cols = table.getColumns(true);

            str.append("    @Override\n");
            str.append("    protected void fillInsertStatement(PreparedStatement ps, ");
            str.append(getTValue());
            str.append(" value) throws SQLException {\n");

            for (int i = 0; i < cols.size(); i++) {
                str.append("        ps.setObject(");
                str.append(i + 1);
                str.append(", value.");
                str.append(cols.get(i).getGetName(true));
                str.append(", Types.");
                str.append(cols.get(i).getType());
                str.append(");\n");
            }

            str.append("    }\n\n");

            // FillUpdateStatement
            str.append("    @Override\n");
            str.append("    protected void fillUpdateStatement(PreparedStatement ps, ");
            str.append(getTValue());
            str.append(" value) throws SQLException {\n");
            str.append("        ps.setObject(");
            str.append(cols.size() + 1);
            str.append(", value.");
            str.append(table.getPrimaryKeyColumn().getGetName(true));
            str.append(", Types.");
            str.append(table.getPrimaryKeyColumn().getType());
            str.append(");\n");

            for (int i = 0; i < cols.size(); i++) {
                str.append("        ps.setObject(");
                str.append(i + 1);
                str.append(", value.");
                str.append(cols.get(i).getGetName(true));
                str.append(", Types.");
                str.append(cols.get(i).getType());
                str.append(");\n");
            }

            str.append("    }\n\n");

            // FillDeleteStatement
            str.append("    @Override\n");
            str.append("    protected void fillDeleteStatement(PreparedStatement ps, ");
            str.append(getTKey());
            str.append(" key) throws SQLException {\n");
            str.append("        ps.setObject(1, key, Types.");
            str.append(table.getPrimaryKeyColumn().getType());
            str.append(");\n    }\n\n");

            // ParseObject
            str.append("    @Override\n");
            str.append("    protected ");
            str.append(getTValue());
            str.append(" parseObject(ResultSet rs) throws SQLException {\n");
            str.append("        var result = new ");
            str.append(getTValue());
            str.append("();\n");

            for (int i = 0; i < cols.size(); i++) {
                str.append("        result.");
                str.append(cols.get(i).getSetName(false));
                str.append("(rs.getObject(\"");
                str.append(cols.get(i).getName());
                str.append("\", ");
                str.append(cols.get(i).getJavaTypeName());
                str.append(".class));\n");
            }

            str.append("        return result;\n");
            str.append("    }\n\n");

            str.append("}\n");

            Utility.dumpToFile(directory + "/" + Utility.pascalCase(this.table.getTableName()) + "DAO.java",
                    str.toString().replace("java.lang.", ""));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}