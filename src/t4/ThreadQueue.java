package t4;

import java.util.ArrayList;

public class ThreadQueue implements Runnable {

    private static int NUMBER_OF_THREADS = 4;

    private static ArrayList<Runnable> jobList = new ArrayList<Runnable>();
    private Thread[] threads;

    public ThreadQueue() {
        threads = new Thread[NUMBER_OF_THREADS];

        for (int i = 0; i < NUMBER_OF_THREADS; i++) {
            threads[i] = new Thread(this);
        }
    }

    public void enqueue(Runnable job) {
        jobList.add(job);
    }

    public void start() {
        for (int i = 0; i < NUMBER_OF_THREADS; i++) {
            threads[i].start();
        }
    }

    public void join() {
        for (int i = 0; i < NUMBER_OF_THREADS; i++) {
            try {
                threads[i].join();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void run() {
        System.out.println("Thread " + Thread.currentThread().getId() + ": Started");
        while (!jobList.isEmpty()) {
            try {
                System.out.println("Thread " + Thread.currentThread().getId() + ": Executing job");
                var job = jobList.get(0);
                jobList.remove(0);
                job.run();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("Thread " + Thread.currentThread().getId() + ": Finished");
    }

}