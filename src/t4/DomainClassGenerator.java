package t4;

public class DomainClassGenerator implements Runnable {

    private TableProxy table;
    private String directory;

    public DomainClassGenerator(TableProxy table, String directory) {
        this.table = table;
        this.directory = directory;
    }

    @Override
    public void run() {
        var pack = Utility.packageFromDirectory(directory);
        var str = new StringBuilder();

        str.append("package ");
        str.append(pack);

        str.append(";\n\n");
        str.append("import javax.annotation.processing.Generated;\n\n");
        str.append("@Generated(\"t4.DomainClassGenerator\")\n");
        str.append("public class ");

        str.append(Utility.pascalCase(this.table.getTableName()));
        str.append(" {\n\n");

        for (var col : this.table.getColumns()) {
            str.append("    private ");
            str.append(col.getJavaTypeName());
            str.append(" ");
            str.append(Utility.camelCase(col.getName()));
            str.append(";\n");
        }

        for (var col : this.table.getColumns()) {
            str.append("\n    public void set");
            str.append(Utility.pascalCase(col.getName()));
            str.append("(");
            str.append(col.getJavaTypeName());
            str.append(" value) {\n        this.");
            str.append(Utility.camelCase(col.getName()));
            str.append(" = value;\n    }\n");

            str.append("\n    public ");
            str.append(col.getJavaTypeName());
            str.append(" get");
            str.append(Utility.pascalCase(col.getName()));
            str.append("() {\n        return this.");
            str.append(Utility.camelCase(col.getName()));
            str.append(";\n    }\n");
        }

        str.append("\n}\n");

        Utility.dumpToFile(directory + "/" + Utility.pascalCase(this.table.getTableName()) + ".java",
                str.toString().replace("java.lang.", ""));

    }

}