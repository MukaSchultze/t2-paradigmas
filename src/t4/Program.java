package t4;

import java.sql.Connection;
import java.sql.SQLException;

public class Program {

    public static void main(String[] args) throws SQLException, Exception {

        Connection connection = DAO.openConnection();

        var metadata = connection.getMetaData();
        var tables = metadata.getTables(null, null, null, new String[] { "TABLE" });

        var threadQueue = new ThreadQueue();

        while (tables.next()) {
            try {
                var tableName = tables.getString("TABLE_NAME");
                var table = new TableProxy(tableName, metadata);
                var classGene = new DomainClassGenerator(table, "src/exported");
                var daoGene = new DaoGenerator(table, "src/exported");
                var exampleGene = new ExampleGenerator(table, "src/exported");

                threadQueue.enqueue(classGene);
                threadQueue.enqueue(daoGene);
                threadQueue.enqueue(exampleGene);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        threadQueue.start();
        System.out.println("Waiting for all threads to finish...");
        threadQueue.join();
        System.out.println("All threads finished");

        connection.close();

    }

}