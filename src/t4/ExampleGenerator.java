package t4;

public class ExampleGenerator implements Runnable {

    private TableProxy table;
    private String directory;

    public ExampleGenerator(TableProxy table, String directory) {
        this.table = table;
        this.directory = directory;
    }

    @Override
    public void run() {
        try {
            var pack = Utility.packageFromDirectory(directory);
            var str = new StringBuilder();

            str.append("package ");
            str.append(pack);

            str.append(";\n\n");
            str.append("import java.sql.SQLException;\n");
            str.append("import java.util.ArrayList;\n\n");
            str.append("import javax.annotation.processing.Generated;\n\n");
            str.append("@Generated(\"t4.ExampleClassGenerator\")\n");
            str.append("public class ");

            str.append(Utility.pascalCase(this.table.getTableName()));
            str.append("Example");
            str.append(" {\n\n");

            str.append("\tpublic static void doExample() {\n");
            str.append("\t\t// Uso de exemplo do DAO\n");
            str.append("\t\tSystem.out.println(\"Testando DAO\");\n\n");
            str.append("\t\t// Criando uma nova instancia\n");

            str.append("\t\tvar dao = new exported.");
            str.append(Utility.pascalCase(this.table.getTableName()));
            str.append("DAO();\n");

            str.append("\t\tArrayList<exported.");
            str.append(Utility.pascalCase(this.table.getTableName()));
            str.append("> todos = null;\n\n");

            str.append("\t\t// Pegando todos elementos da tabela\n");
            str.append("\t\tSystem.out.println(\"Selecionando todos...\");\n");
            str.append("\t\ttry {\n");
            str.append("\t\t\ttodos = dao.selectAll();\n");
            str.append("\t\t} catch (SQLException e) {\n");
            str.append("\t\t\te.printStackTrace();\n");
            str.append("\t\t}\n\n");

            str.append("\t\t// Pega um elemento aleatório do conjunto total\n");
            str.append("\t\tvar rand = randomFrom(todos);\n\n");

            str.append("\t\t// Remove este elemento\n");
            str.append("\t\ttry {\n");
            str.append("\t\t\tdao.delete(rand.");
            str.append(table.getPrimaryKeyColumn().getGetName(true));
            str.append(");\n");
            str.append("\t\t} catch (SQLException e) {\n");
            str.append("\t\t\te.printStackTrace();\n");
            str.append("\t\t}\n\n");

            str.append("\t\t// Reinsere o elemento removido\n");
            str.append("\t\tSystem.out.println(\"Reinserindo elemento...\");\n");
            str.append("\t\ttry {\n");
            str.append("\t\t\tdao.insert(rand);\n");
            str.append("\t\t} catch (SQLException e) {\n");
            str.append("\t\t\te.printStackTrace();\n");
            str.append("\t\t}\n\n");

            str.append("\t\t// Testa pra ver se encontra no banco\n");
            str.append("\t\t// System.out.println(\"Selecionando elemento...\");\n");
            str.append("\t\ttry {\n");
            str.append("\t\t\trand = dao.select(rand.");
            str.append(table.getPrimaryKeyColumn().getGetName(true));
            str.append(");\n");
            str.append("\t\t} catch (SQLException e) {\n");
            str.append("\t\t\te.printStackTrace();\n");
            str.append("\t\t}\n");

            str.append("\t\tSystem.out.println(\"Fim dos testes...\");\n");
            str.append("\t\t}\n\n");

            str.append("\tprivate static void printItem(exported.");
            str.append(Utility.pascalCase(this.table.getTableName()));
            str.append(" item) {\n");
            str.append("\t\tSystem.out.println(\"Item: \" + item.");
            str.append(table.getPrimaryKeyColumn().getGetName(true));
            str.append(");\n");
            str.append("\t}\n\n");

            str.append("\tprivate static <T> T randomFrom(ArrayList<T> arr) {\n");
            str.append("\t\treturn arr.get((int) (Math.random() * arr.size()));\n");
            str.append("\t}\n\n}");

            Utility.dumpToFile(directory + "/" + Utility.pascalCase(this.table.getTableName()) + "Example.java",
                    str.toString().replace("java.lang.", ""));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}