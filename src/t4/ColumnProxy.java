package t4;

import java.math.BigDecimal;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.JDBCType;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Struct;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;

public class ColumnProxy {

    private String name;
    private int typeNumeric;
    private JDBCType type;
    private boolean nullable;
    private boolean isPrimaryKey;

    public String getName() {
        return this.name;
    }

    public JDBCType getType() {
        return this.type;
    }

    public boolean getNullable() {
        return this.nullable;
    }

    public boolean isTypeSupported() {
        return !this.getJavaTypeName().equals("java.lang.Object");
    }

    public String getJavaTypeName() {
        switch (this.typeNumeric) {
        case Types.CHAR:
        case Types.VARCHAR:
        case Types.LONGVARCHAR:
            return String.class.getName();
        case Types.BIT:
            return Boolean.class.getName();
        case Types.NUMERIC:
        case Types.DECIMAL:
            return BigDecimal.class.getName();
        case Types.TINYINT:
            return Byte.class.getName();
        case Types.SMALLINT:
            return Short.class.getName();
        case Types.INTEGER:
            return Integer.class.getName();
        case Types.BIGINT:
            return Long.class.getName();
        case Types.FLOAT:
        case Types.REAL:
        case Types.DOUBLE:
            return Double.class.getName();
        case Types.DATE:
            return Date.class.getName();
        case Types.TIME:
            return Time.class.getName();
        case Types.TIMESTAMP:
            return Timestamp.class.getName();
        case Types.BINARY:
        case Types.VARBINARY:
        case Types.LONGVARBINARY:
            return "Byte[]"; // Byte[].class.getName();
        case Types.STRUCT:
            return Struct.class.getName();
        case Types.ARRAY:
            return Array.class.getName();
        case Types.BLOB:
            return Blob.class.getName();
        case Types.CLOB:
            return Clob.class.getName();
        case Types.REF:
            return Ref.class.getName();
        case Types.NULL:
        case Types.OTHER:
        case Types.JAVA_OBJECT:
        case Types.DISTINCT:
        case Types.DATALINK:
        case Types.BOOLEAN:
        case Types.ROWID:
        case Types.NCHAR:
        case Types.NVARCHAR:
        case Types.LONGNVARCHAR:
        case Types.NCLOB:
        case Types.SQLXML:
        case Types.REF_CURSOR:
        case Types.TIME_WITH_TIMEZONE:
        case Types.TIMESTAMP_WITH_TIMEZONE:
        default:
            System.err.println("Type not supported: " + this.type.getName() + " column " + this.name);
            return Object.class.getName();
        }
    }

    public String getFieldName() {
        return Utility.camelCase(getName());
    }

    public String getGetName(boolean withParenthesis) {
        if (withParenthesis)
            return getGetName(false) + "()";
        else
            return "get" + Utility.pascalCase(getName());
    }

    public String getSetName(boolean withParenthesis) {
        if (withParenthesis)
            return getSetName(false) + "()";
        else
            return "set" + Utility.pascalCase(getName());
    }

    public boolean getIsPrimaryKey() {
        return this.isPrimaryKey;
    }

    public void setIsPrimaryKey(boolean value) {
        this.isPrimaryKey = value;
    }

    public ColumnProxy(ResultSet columns) {
        try {
            this.name = columns.getString("COLUMN_NAME");
            this.typeNumeric = columns.getInt("DATA_TYPE");
            this.type = JDBCType.valueOf(this.typeNumeric);
            this.nullable = columns.getBoolean("IS_NULLABLE");
            // var columnsize = field.getString("COLUMN_SIZE");
            // var decimaldigits = field.getString("DECIMAL_DIGITS");
            // var is_autoIncrment = field.getString("IS_AUTOINCREMENT");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected ColumnProxy() {
    }

}