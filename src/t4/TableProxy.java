package t4;

import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

public class TableProxy {

    private String tableName;
    private ArrayList<ColumnProxy> columns;

    public String getTableName() {
        return this.tableName;
    }

    public ArrayList<ColumnProxy> getColumns() {
        return this.columns;
    }

    public ArrayList<ColumnProxy> getColumns(boolean includePrimaryKey) {
        return this.columns;
    }

    public ColumnProxy getPrimaryKeyColumn() throws Exception {
        for (var col : getColumns())
            if (col.getIsPrimaryKey())
                return col;
        throw new Exception("Table doesn't have a primary key");
    }

    public TableProxy(String tableName, DatabaseMetaData metadata) {
        try {
            this.tableName = tableName;
            this.columns = new ArrayList<>();

            var columnsRS = metadata.getColumns(null, null, tableName, null);
            var primaryKeys = metadata.getPrimaryKeys(null, null, tableName);

            while (columnsRS.next()) {
                this.columns.add(new ColumnProxy(columnsRS));
            }

            var setPrimaryKey = false;

            while (primaryKeys.next()) {
                var primaryKeyName = primaryKeys.getString("COLUMN_NAME");

                for (int i = 0; i < this.columns.size(); i++) {
                    var col = this.columns.get(i);

                    if (primaryKeyName.equals(col.getName())) {
                        col.setIsPrimaryKey(true);

                        if (setPrimaryKey)
                            System.err.println("Table has more than one primary key: " + tableName);

                        setPrimaryKey = true;
                    }
                }
            }

            for (int i = 0; i < this.columns.size(); i++)
                if (!this.columns.get(i).isTypeSupported())
                    this.columns.remove(i--);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}