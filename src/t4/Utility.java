package t4;

import java.io.File;
import java.io.PrintWriter;

public class Utility {

    public static String pascalCase(String className) {
        className = className.toLowerCase();
        className = className.replace("_", " ");
        className = switchCase(className, 0, true);

        while (className.contains(" ")) {
            var index = className.indexOf(" ");
            className = className.replaceFirst(" ", "");
            className = switchCase(className, index, true);
        }

        return className;
    }

    public static String camelCase(String className) {
        className = className.toLowerCase();
        className = className.replace("_", " ");

        while (className.contains(" ")) {
            var index = className.indexOf(" ");
            className = className.replaceFirst(" ", "");
            className = switchCase(className, index, true);
        }

        return className;
    }

    public static String switchCase(String str, int charIndex, boolean uppercase) {
        if (charIndex >= str.length())
            return str;

        var ch = str.charAt(charIndex);
        var pre = str.substring(0, charIndex);
        var post = str.substring(charIndex + 1);

        if (uppercase)
            ch = Character.toUpperCase(ch);
        else
            ch = Character.toLowerCase(ch);

        return pre + ch + post;
    }

    public static String packageFromDirectory(String directory) {
        directory = directory.replace("\\", "/");
        var index = directory.lastIndexOf("/");
        return index == -1 ? directory : directory.substring(index + 1);
    }

    public static void dumpToFile(String path, String contents) {
        try {
            var file = new File(path);
            var dir = new File(file.getParent());

            dir.mkdir();
            file.createNewFile();

            try (var writer = new PrintWriter(file)) {
                writer.write(contents);
                System.out.println("Wrote file: " + file.getName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}