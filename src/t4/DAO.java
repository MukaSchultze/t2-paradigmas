package t4;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public abstract class DAO<TValue, TKey> {

    protected abstract String getSelectSQL();

    protected abstract String getSelectAllSQL();

    protected abstract String getInsertSQL();

    protected abstract String getUpdateSQL();

    protected abstract String getDeleteSQL();

    protected abstract void fillSelectStatement(PreparedStatement ps, TKey key) throws SQLException;

    protected abstract void fillInsertStatement(PreparedStatement ps, TValue value) throws SQLException;

    protected abstract void fillUpdateStatement(PreparedStatement ps, TValue value) throws SQLException;

    protected abstract void fillDeleteStatement(PreparedStatement ps, TKey key) throws SQLException;

    protected abstract TValue parseObject(ResultSet rs) throws SQLException;

    public static Connection openConnection() {
        Connection c = null;

        try {
            Class.forName(Constants.SQL_JDBC_DRIVER);
        } catch (ClassNotFoundException e) {
            System.out.println("Driver não encontrado " + Constants.SQL_JDBC_DRIVER);
        }

        try {
            c = DriverManager.getConnection(Constants.SQL_URL_CONEXAO, Constants.SQL_USER, Constants.SQL_PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;

    }

    public TValue select(TKey key) throws SQLException {
        TValue result = null;

        var connection = openConnection();
        var ps = connection.prepareStatement(getSelectSQL());

        fillSelectStatement(ps, key);

        var rs = ps.executeQuery();

        if (rs.next())
            result = parseObject(rs);

        rs.close();
        ps.close();
        connection.close();

        return result;
    }

    public ArrayList<TValue> selectAll() throws SQLException {
        var result = new ArrayList<TValue>();
        var connection = openConnection();
        var ps = connection.prepareStatement(getSelectAllSQL());
        var rs = ps.executeQuery();

        while (rs.next())
            result.add(parseObject(rs));

        rs.close();
        ps.close();
        connection.close();

        return result;
    }

    public void insert(TValue value) throws SQLException {
        var connection = openConnection();
        var ps = connection.prepareStatement(getInsertSQL());

        fillInsertStatement(ps, value);

        ps.execute();
        ps.close();
        connection.close();
    }

    public void update(TValue value) throws SQLException {
        var connection = openConnection();
        var ps = connection.prepareStatement(getUpdateSQL());

        fillUpdateStatement(ps, value);

        ps.execute();
        ps.close();
        connection.close();
    }

    public void delete(TKey key) throws SQLException {
        var connection = openConnection();
        var ps = connection.prepareStatement(getDeleteSQL());

        fillDeleteStatement(ps, key);

        ps.execute();
        ps.close();
        connection.close();
    }

}